class Edge(dict):
    _keys = ['source', 'target']

    def __init__(self, **kwargs):
        for key in set(self._keys) - set(kwargs.keys()):
            if key == 'source' or key == 'target':
                self[key] = ''

        for key in kwargs:
            self[key] = kwargs[key]

    def calculate_span(self, layering):
        i, j = None, None
        for layer_index, layer in enumerate(layering):
            if self["source"] in layer:
                i = layer_index
            if self["target"] in layer:
                j = layer_index
        return j - i

    def set_span(self, span=None, layering=None):
        if span is None and layering is not None:
            span = self.calculate_span(layering)
        if type(span) == int:
            self["span"] = span

    def vw(self):
        return self['source'], self['target']

    def reverse(self):
        v, w = self.vw()
        try:
            if self["reversed"]:
                self["reversed"] = False
                self["source"] = w
                self["target"] = v
            else:
                self["reversed"] = True
                self["source"] = w
                self["target"] = v
        except KeyError:
            self["reversed"] = True
            self["source"] = w
            self["target"] = v

    def span(self):
        try:
            return self['span']
        except KeyError:
            print('KeyError: calling Edge has no key "span". None returned')
            return None

    def needs_dummy_q(self):
        if self.span() > 1:
            return True
        else:
            return False

    def dummy_q(self):
        try:
            markers = ['r_', 's_', 'p_', 'q_', 'dummy']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            print('KeyError: calling Edge has no key "class". None returned.')
            return None

    def p_edge(self):
        try:
            markers = ['p_']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            print('KeyError: calling Edge has no key "class". None returned.')
            return None

    def reversed(self):
        try:
            return self['reversed']
        except KeyError:
            print('KeyError: calling Edge has no key "reversed". None returned.')

    def connection_type(self):
        try:
            return self['connection_type']
        except KeyError:
            return None
            print('KeyError: calling Edge has no key "connection_type". None returned.')
