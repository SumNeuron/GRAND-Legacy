import pickle
import os
import sys

if __name__ == '__main__':
    from Graph import Graph
else:
    from .Graph import Graph

sys.modules['Graph'] = Graph


def test_dump():
    g = Graph()
    write_file = open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/test.p', 'wb')
    pickle.dump(g, write_file)

def test_load():
    g = pickle.load(open( '/home/sumner/PycharmProjects/GRAND/Pathway_Commons/test.p','rb'))
    return g

def PathwayCommonsMatrix():
    file = open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/PathwayCommons_preprocessed.txt', "r")

    proteins = []
    edges = []
    connections = []

    for ind, line in enumerate(file):
        print('on line ', ind)
        source, connection, target = line.rstrip('\n').split('\t')
        if source not in proteins:
            proteins.append(source)
        if target not in proteins:
            proteins.append(target)

        connections.append(connection)
        edges.append([source, target])

    matrix = [[0 for j in proteins] for i in proteins]

    for edge in edges:
        source, target = edge
        matrix[proteins.index(source)][proteins.index(target)] = 1

    file.close()
    return matrix, proteins, edges, connections


def pickle_pathway_commons():
    print('loading matrix')
    matrix, P, E, C = PathwayCommonsMatrix()

    print('dumping files')
    pickle.dump(matrix, open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/pickled_pathway_commons_matrix.p', 'wb'))
    pickle.dump(P, open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/pickled_pathway_commons_proteins.p', 'wb'))
    pickle.dump(E, open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/pickled_pathway_commons_edges.p', 'wb'))
    pickle.dump(C, open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/pickled_pathway_commons_connections.p', 'wb'))
    G = Graph(P, E)
    G.edge_set.add_subkey('connection_type', '')

    for i, (v, w) in enumerate(E):
        G.edge_set[str(v)+'_'+str(w)]['connection_type'] += C[i]


    pickle.dump(G, open('/home/sumner/PycharmProjects/GRAND/Pathway_Commons/pickled_pathway_commons_graph.p', 'wb'))


def load_pickled_pathway_commons_matrix(location):
    if location is not None:
        return pickle.load(open(location, 'rb'))
    return pickle.load(open('./Pathway_Commons/pickled_pathway_commons_matrix.p', 'rb'))

def load_pickled_pathway_commons_graph(location):
    if location is not None:
        return pickle.load(open(location, 'rb'))

    return pickle.load(open('./Pathway_Commons/pickled_pathway_commons_graph.p', 'rb'))




# pickle_pathway_commons()
