from OrderedNestedDictionary import OrderedNestedDictionary


class EdgeSet(OrderedNestedDictionary):
    def __init__(self, *args, **kwargs):
        super(EdgeSet, self).__init__(*args, **kwargs)

    def __set_spans__(self, layering):
        for edge_name, edge_values in self.items():
            edge_values.set_span(layering=layering)

    def __edges_between_strongly_connected_components__(self, strongly_connected_components):
        edges_between = []
        for edge_key, edge_values in self.items():
            u, w = edge_values.vw()
            for current, component_1 in enumerate(strongly_connected_components):
                for component_2 in strongly_connected_components[current:]:

                    if component_1 == component_2:
                        continue

                    if (u in component_1 and w in component_2) or (w in component_1 and u in component_2):
                        edges_between.append(edge_key)

        return edges_between

    def __feedback_set_from_linear_ordering__(self, vertex_ordering):  # set of edges whose reversal makes a DAG
        edges_to_reverse = []
        for edge_key, edge in self.items():
            u, w = edge.vw()
            if vertex_ordering.index(u) > vertex_ordering.index(w):
                edges_to_reverse.append(edge_key)
        return edges_to_reverse

    def __downward_edges_of_layer__(self, layer):
        return self.select(**{'source': lambda source: source in layer})

    def __upward_edges_of_layer__(self, layer):
        return self.select(**{'target': lambda target: target in layer})

    def __make_layering_keys__(self):
        for key in ['span', 'directed', "reversed"]:
            self.add_subkey(key)

    def __make_dummy_keys__(self):
        self.add_subkey('class', 'regular')

    def __make_crossing_type_keys__(self):
        self.add_subkey('crossing_types', {'type_0': None, 'type_1': None, 'type_2': None})

    def __remove_dummy_keys__(self):
        for key in ['class','crossing_types']:
            self.delete_subkey(key)

    def __remove_layering_keys__(self):
        for key in ['span', 'directed', "reversed"]:
            self.delete_subkey(key)

    def __remove_crossing_types_keys__(self):
        self.delete_subkey('crossing_types')

    def s_edges(self):
        return self.select(**{'class': 's_edge'})

    def p_edges(self):
        return self.select(**{'class': 'p_edge'})

    def q_edges(self):
        return self.select(**{'class': 'q_edge'})

    def reverse(self, edge_key):
        source_key, target_key = edge_key.split('_')[0], edge_key.split('_')[-1]
        edge = self[edge_key]

        new_edge_key = target_key + "_" + source_key
        i = 1
        while new_edge_key in self.keys():
            i += 1
            new_edge_key = target_key + i * "_" + source_key

        edge['source'] = target_key
        edge['target'] = source_key
        edge['reversed'] = True
        self[new_edge_key] = edge

        del self[edge_key]
