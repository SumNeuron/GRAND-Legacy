class Vertex(dict):
    _keys = ['predecessors', 'successors']

    def __init__(self, **kwargs):

        for key in set(self._keys) - set(kwargs.keys()):
            if key == 'predecessors' or key == 'successors':
                self[key] = []

        for key in kwargs:
            self[key] = kwargs[key]

    def has_key_q(self, key):
        if key in self:
            return True
        return False

    def __incident_edge_reversed__(self, other_vertex, others_relation='source'):
        if self.has_key_q('in_degree'):
            if others_relation == "target":
                self["out_degree"] -= 1
                self["in_degree"] += 1
                self["successors"].pop(self["successors"].index(str(other_vertex)))
                self["predecessors"].append(str(other_vertex))

            elif others_relation == "source":
                self["out_degree"] += 1
                self["in_degree"] -= 1
                self["predecessors"].pop(self["predecessors"].index(str(other_vertex)))
                self["successors"].append(str(other_vertex))
        else:
            if others_relation == "target":
                self["successors"].pop(self["successors"].index(str(other_vertex)))
                self["predecessors"].append(str(other_vertex))

            elif others_relation == "source":
                self["predecessors"].pop(self["predecessors"].index(str(other_vertex)))
                self["successors"].append(str(other_vertex))

    def __strong_connect__(self, vertex_set, tarjan_index, stack, strongly_connected_components):
        self['tarjan_index'] = tarjan_index
        self['tarjan_low_link'] = tarjan_index
        tarjan_index += 1
        stack.append(self)
        self['on_tarjan_stack'] = True

        for w in self["successors"]:
            if vertex_set[w]["tarjan_index"] is None:
                vertex_set[w].__strong_connect__(vertex_set, tarjan_index, stack, strongly_connected_components)
                self["tarjan_low_link"] = min(self["tarjan_low_link"], vertex_set[w]["tarjan_low_link"])

            elif vertex_set[w]["on_tarjan_stack"] is True:
                self["tarjan_low_link"] = min(self["tarjan_low_link"], vertex_set[w]["tarjan_index"])

        if self["tarjan_low_link"] == self["tarjan_index"]:
            strongly_connected_component = []

            flag = True
            while flag:
                w = stack.pop()
                w["on_tarjan_stack"] = False
                strongly_connected_component.append(w)

                if w == self:
                    flag = False

            strongly_connected_components.append(strongly_connected_component)

    def out_degree(self):
        return len(self['successors'])

    def in_degree(self):
        return len(self['predecessors'])

    def degree(self):
        return len(self['successors']) + len(self['predecessors'])

    def successors(self):
        return self['successors']

    def predecessors(self):
        return self['predecessors']

    def dummy_q(self):
        try:
            markers = ['r_', 's_', 'p_', 'dummy']
            for marker in markers:
                if marker in self['class']:
                    return True
            return False
        except KeyError:
            # print('KeyError: calling Vertex has no key "class". None returned.')
            return False

    def __edges__(self, id):
        edges = []
        for successor in self['successors']:
            edges += [id + '_' + successor]
        for predecessor in self['predecessors']:
            edges += [predecessor + '_' + id]
        return edges
