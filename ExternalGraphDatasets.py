import os

def parse_graph_file(file):
    f = open(file, 'r')
    vertices = []
    edges = []
    for line in f.readlines():
        if 'node' in line:
            garbage, node = line.split('id=')
            node, garbage = node.split('/')
            vertices.append(node)

        if '<edge' in line:
            garbage, source = line.split('source=')
            source, target = source.split(' target=')
            target, garbage = target.split('/')
            edges.append([source, target])


    vertices = [v.replace('"', '') for v in vertices]
    edges = [[s.replace('"', ''), t.replace('"', '')] for (s, t) in edges]
    f.close()
    return vertices, edges


def directory_of(dataset='att'):
    here = os.path.dirname(os.path.realpath(__file__))
    files_location = os.path.join(here, dataset)
    return files_location


def get_files(directory):
    files = os.listdir(directory)
    files = [os.path.join(directory, file) for file in files]
    return files


def make_directory_of_graphs(files):
    graphs = {}
    here = os.path.dirname(os.path.realpath(__file__))
    for i, file in enumerate(files):
        if '.log' in file:
            continue
        v, e = parse_graph_file(file)
        graphs[i] = (v, e)
    return graphs


def retrieve_external_dataset(dataset='att'):
    directory = directory_of(dataset)
    files = get_files(directory)
    graphs = make_directory_of_graphs(files)
    return graphs


def output(files, headers, values):
    here = os.path.dirname(os.path.realpath(__file__))
    output_files = [os.path.join(here, file) for file in files]

    for i, file in enumerate(output_files):
        f = open(file, 'w')

        header_string = ''
        for j, header in enumerate(headers):
            if j == len(headers) - 1:
                header_string += header + '\n'
            else:
                header_string += header + '\t'

        f.write(header_string)

        for value_tuple in values[i]:
            value_string = ''
            for k, value in enumerate(value_tuple):
                if k == len(value_tuple) - 1:
                    value_string += str(value) + '\n'
                else:
                    value_string += str(value) + '\t'
            f.write(value_string)

        f.close()
