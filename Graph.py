import math
from copy import copy, deepcopy
from itertools import chain, groupby
from math import floor, ceil
from operator import itemgetter
from random import shuffle
from statistics import median_high, median_low


from Edge import Edge
from EdgeSet import EdgeSet
from SVG import SVG
from Vertex import Vertex
from VertexSet import VertexSet



class Graph:
    def __init__(self, vertices = [], edges = []):
        """
        Initialize a graph object. The easiest** way to make a graph is to give it a list of vertices (just IDs) and
        a list of lists representing edges. If the edges are meant to be ordered, then they should be input as such
        e.g. an edge[i,j] represents (i o---> j).

        ** this holds if you are transferring data / output from another program or function. Naturally, one can
        also give the graph the VertexSet and EdgeSet objects. Alternatively a list of Vertex / Edge objects will
        suffice.
        """

        # Logic Flow:
        # Test if the arguments vertices and edges are lists.
        # ---- If: the first element in each list are Vertex or Edge object, pass them to VertexSet and EdgeSet
        #      constructors.
        # ---- Else: convert list of IDs into Vertex and Edge Objects
        # Else: arguments are VertexSet and EdgeSet objects.

        # Default arguments which are empty.
        if not vertices and not edges:
            self.vertex_set,  self.edge_set = VertexSet(), EdgeSet()
            self.layering, self.removed_edges = [], EdgeSet()

        # Passing VertexSet and EdgeSet
        elif type(vertices) == VertexSet and type(edges) == EdgeSet:
            self.vertex_set, self.edge_set = vertices, edges
            self.layering, self.removed_edges = [], EdgeSet()

        # Passed named tuples
        elif (type(vertices[0]) == tuple and type(edges[0]) == tuple) and \
             (type(vertices[0][1]) == Vertex and type(edges[0][1]) == Edge):

            to_add = {}
            for vertex_tuple in vertices:
                to_add[vertex_tuple[0]] = vertex_tuple[1]
            self.vertex_set = VertexSet(**to_add)

            to_add = {}
            for edge_tuple in edges:
                to_add[edge_tuple[0]] = edge_tuple[1]
            self.edge_set = EdgeSet(**to_add)

        # Passed arguments are not empty, not a VertexSet or EdgeSet nor named tuples
        elif type(vertices[0]) == Vertex and type(edges[0]) == Edge:
            try:
                to_add = {}
                for vertex in vertices:
                    to_add[vertex['name']] = vertex
                self.vertex_set = VertexSet(**to_add)
            except KeyError:
                print("Vertices did not contain a 'name' key. Vertices initialized with"
                      " index based 'names'.")
                to_add = {}
                for index, vertex in enumerate(vertices):
                    to_add[index] = vertex
                self.vertex_set = VertexSet(**to_add)

            try:
                to_add = {}
                for edge in edges:
                    edge_key = edge['source'] + "_" + edge['target']
                    i = 1
                    while edge_key in to_add.keys():
                        i += 1
                        edge_key = edge['source'] + i * '_' + edge['target']
                    to_add[edge_key] = edge
                self.edge_set = EdgeSet(**to_add)
            except KeyError:
                print("Edges do not have complete information. Initializing an empty edge set instead.")
                self.edge_set = EdgeSet()

            self.layering = []
            self.removed_edges = EdgeSet()

        # Assume then it is just lists of strings and such
        else:
            self.vertex_set = VertexSet()
            self.edge_set = EdgeSet()
            for vertex in vertices:
                self.vertex_set[str(vertex)] = Vertex()

            edges = self.preprocess_input(vertices, edges)
            for edge in edges:
                v, w = edge[0], edge[1]
                edge_key = str(v) + "_" + str(w)
                i = 1
                while edge_key in self.edge_set:
                    i += 1
                    edge_key = str(v) + i * '_' + str(w)
                self.edge_set[edge_key] = Edge(**{'source':str(v), 'target':str(w)})

                self.vertex_set[str(v)]['successors'].append(str(w))
                self.vertex_set[str(w)]['predecessors'].append(str(v))

            self.layering = []
            self.removed_edges = EdgeSet()

        self.number_of_crossings = None
        self.number_of_layers = 0



    def preprocess_input(self, vertices, edges):
        # removes self loops

        buckles = [[u, w] for (u, w) in edges if u == w]
        edges = [edge for edge in edges if edge not in buckles]
        excluded_edges = []

        tallied_edges = self.tally(edges)
        for edge_key, tally in tallied_edges:
            if tally > 1:
                [excluded_edges.append(edge_key) for i in range(tally - 1)]

        edges = [key for key, iterator_thing in groupby(edges)]

        self.buckles = buckles
        self.excluded_edges = excluded_edges
        # print('buckles:', buckles)
        # print('excluded edges:', excluded_edges)
        return edges


    def tally(self, array):
        items_in_list = [key for key, iterator_thing in groupby(array)]
        tally = []
        for item in items_in_list:
            counter = 0
            for other in items_in_list:
                if item == other:
                    counter += 1
            tally.append([item, counter])
        return tally





    def set_spans(self):
        """
        ASSUMES that the a layering has been made. Accordingly sets the span for all edges in the EdgeSet.
        :return: None
        """
        if self.layering is not []:
            try:
                self.edge_set.first['span']
            except KeyError:
                self.edge_set.__make_layering_keys__()
            self.edge_set.__set_spans__(self.layering)

    def remove_edge(self, edge_key):

        # In both: remove from edge_set
        if edge_key in self.edge_set and edge_key in self.removed_edges:
            edge = self.edge_set.pop(edge_key)
            u, w = edge.vw()

            # If the edges are not equivalent, update second edge.
            if edge != self.removed_edges[edge_key]:
                self.removed_edges[edge_key] = edge

            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass

            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # In edge_key and not in removed edges
        elif edge_key in self.edge_set and edge_key not in self.removed_edges:
            # Remove from edge_set and add to removed edges
            edge = self.edge_set.pop(edge_key)
            u, w = edge.vw()


            self.removed_edges[edge_key] = edge

            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass

            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # Not in edge_key but in removed edges
        elif edge_key not in self.edge_set and edge_key in self.removed_edges:
            # Edge already removed. Test for safety
            edge = self.removed_edges[edge_key]
            u, w = edge.vw()
            # Update successors and predecessors
            if w in self.vertex_set[u]['successors']:
                self.vertex_set[u]['successors'].remove(w)
                try:
                    self.vertex_set[u]['out_degree'] -= 1
                except KeyError:
                    pass
            if u in self.vertex_set[w]['predecessors']:
                self.vertex_set[w]['predecessors'].remove(u)
                try:
                    self.vertex_set[w]['in_degree'] -= 1
                except KeyError:
                    pass

        # Not in either
        else:
            pass
            # print("Edge: ", edge_key, " was not found in either the EdgeSet or the RemovedEdges.")

    def remove_vertex(self, vertex_key):
        vertex = self.vertex_set[vertex_key]

        edges = vertex.__edges__(vertex_key)

        for edge in edges:
            self.remove_edge(edge)

    def reverse_edge(self, edge_key):
        """
        If one wants to reverse an edge, this function does so. Further it also updates the affected vertices so that
        the successors and predecessors are correct.
        :param edge_key: a string for querying the EdgeSet
        :return: None
        """
        edge_key = self.edge_to_edge_key(edge_key)
        edge = self.edge_set[edge_key]
        u, w = edge.vw()
        edge.reverse()

        self.vertex_set[u].__incident_edge_reversed__(str(w), "target")
        self.vertex_set[w].__incident_edge_reversed__(str(u), "source")


    def reverse_edge(self, edge_key):
        """
        If one wants to reverse an edge, this function does so. Further it also updates the affected vertices so that
        the successors and predecessors are correct.
        :param edge_key: a string for querying the EdgeSet
        :return: None
        """


        self.edge_set.reverse(edge_key)
        source, target = edge_key.split('_')[0], edge_key.split('_')[-1]
        self.vertex_set[source].__incident_edge_reversed__(str(target), "target")
        self.vertex_set[target].__incident_edge_reversed__(str(source), "source")


    def edge_to_edge_key(self, edge):
        string = ''
        if type(edge) == Edge:
            u, w = edge.vw
            string = str(u) + "_" + str(w)
        elif type(edge) == list:
            u, w = edge[0], edge[1]
            string = str(u) + "_" + str(w)
        elif type(edge) == str:
            string = edge
        return string

    # Methods for removing cylces
    def feedback_set(self, implementation='berger_shor_tarjan_modified'):

        #Remove edges between strongly connected components
        if implementation == 'berger_shor_tarjan_modified':
            self.vertex_set.__make_tarjan_keys__()
            vertex_set = deepcopy(self.vertex_set)
            edge_set = deepcopy(self.edge_set)
            graph = Graph(vertex_set, edge_set)
            edges_to_reverse = self.__berger_shor__(graph, implementation)
            del vertex_set
            del edge_set
            del graph

        # Linear Ordering Methods
        if implementation == 'eades_et_al':
            vertex_ordering = self.vertex_set.__eades_et_al__()
            edges_to_reverse = self.edge_set.__feedback_set_from_linear_ordering__(vertex_ordering)

        if implementation == 'magruder_magnitude':
            vertex_ordering = self.vertex_set.__magruder_magnitute__()
            edges_to_reverse = self.edge_set.__feedback_set_from_linear_ordering__(vertex_ordering)

        return edges_to_reverse

    def feedback_arc_set(self, implementation='berger_shor'):
        if implementation == 'berger_shor':
            vertex_set = copy.deepcopy(self.vertex_set)
            edge_set = copy.deepcopy(self.edge_set)
            graph = Graph(vertex_set, edge_set)
            edges_to_delete = self.__berger_shor__(graph)

        return edges_to_delete

    def remove_cycles(self, method='feedback_set', implementation='berger_shor_tarjan_modified'):
        _methods = ['feedback_set', 'feedback_arc_set', 'linear_ordering']
        _implementations = ['eades_et_al', 'berger_shor', 'berger_shor_tarjan_modified']

        if method == 'feedback_set':
            edges_to_reverse = self.feedback_set(implementation)
            if edges_to_reverse:
                for edge in edges_to_reverse:
                    self.reverse_edge(edge)

        if method == 'feedback_arc_set':
            edges_to_remove = self.feedback_arc_set(implementation)
            if edges_to_remove:
                for edge in edges_to_remove:
                    self.remove_edge(edge)

    def __berger_shor__(self, graph, implementation=None):

        acyclic_edges = []
        cyclic_edges = []

        if implementation == "minimum_feedback_arc_set":
            strongly_connected_components = graph.vertex_set.__tarjan_strongly_connected_components__()
            edges_between = graph.edge_set.__edges_between_strongly_connected_components__(strongly_connected_components)

            for edge in edges_between:
                if self.edge_set[edge]['source'] == self.edge_set[edge]['target']:
                    continue
                acyclic_edges.append(edge)
                graph.remove_edge(edge)

        if len(graph.edge_set) is not 0:  ##if all edges removed because graph acylcic no need to run
            for vertex_name, vertex_values in graph.vertex_set.items():

                if vertex_values.out_degree() >= vertex_values.in_degree():
                    for target in vertex_values["successors"]:
                        if target == vertex_name:
                            continue
                        else:
                            acyclic_edges.append(vertex_name + "_" + target)
                else:
                    for source in vertex_values["predecessors"]:
                        acyclic_edges.append(source + "_" + vertex_name)
                graph.remove_vertex(vertex_name)

        for edge_key in self.edge_set:
            if edge_key not in acyclic_edges:
                cyclic_edges.append(edge_key)


        return cyclic_edges


    # Methods for making a layering
    def make_layering(self, method='healy_nikolov', implementation=''):
        layering = self.vertex_set.__make_layering__(method=method, implementation=implementation)
        self.layering = layering
        self.number_of_layers = len(layering)

        self.set_spans()


    # Methods for making dummy vertices and edges
    def generate_dummies(self, method='sugiyama'):
        _methods = ['efficient_sugiyama', 'sugiyama']
        self.edge_set.__make_dummy_keys__()
        self.vertex_set.__make_dummy_keys__()

        if method == 'efficient_sugiyama':
            self.efficient_sugiyama()
        elif method == 'sugiyama':
            self.sugiyama()

    def sugiyama(self):
        # Make a copy of the edge set to play with and remove edges from without worry.
        # Further prevents going through new edges. No need to worry about those.
        original_edge_set = deepcopy(self.edge_set)
        original_edge_set_keys = list(original_edge_set.keys())

        # Go through the edges in the original edge_set
        for edge in original_edge_set_keys:
            if original_edge_set[edge].needs_dummy_q():
                u, w = self.edge_set[edge].vw()
                span = self.edge_set[edge]['span']
                old_edge = self.edge_set[edge]

                source_layer, target_layer = self.vertex_set[u]['layer'], self.vertex_set[w]['layer']
                self.remove_edge(edge)

                # while the span is greater than one we need to collectively add edges
                # until the graph is properly layered. When the the span is one then we can use the original target
                current_source = u
                layer_index = 0
                while span is not 1:

                    # Bookkeeping
                    span -= 1
                    layer_index += 1

                    # Ensure not overwriting an existing vertex key
                    new_vertex = str(len(self.vertex_set) + 1)
                    while new_vertex in self.vertex_set:
                        new_vertex = str(int(new_vertex) + 1)

                    # Add edge
                    new_edge = current_source + "_" + new_vertex
                    if self.vertex_set[current_source]['class'] == 'regular':
                        new_edge_object = deepcopy(old_edge)
                        new_edge_object['source'] = current_source
                        new_edge_object['target'] = new_vertex
                        new_edge_object['class'] = 'p_edge'
                        self.edge_set[new_edge] = new_edge_object
                    else:
                        new_edge_object = deepcopy(old_edge)
                        new_edge_object['source'] = current_source
                        new_edge_object['target'] = new_vertex
                        new_edge_object['class'] = 'inner_segment_dummy_edge'
                        self.edge_set[new_edge] = new_edge_object

                    # Update previous vertex successors and make new vertex
                    self.vertex_set[current_source]['successors'].append(new_vertex)
                    self.vertex_set[new_vertex] = Vertex(**{'name': new_vertex, 'predecessors': [current_source],
                                                            'layer': source_layer + layer_index,
                                                            'position_in_layer': len(self.layering[source_layer + layer_index]),
                                                            'class': 'dummy_vertex'})

                    # Add new vertex to layer.
                    self.layering[source_layer + layer_index].append(new_vertex)
                    current_source = new_vertex

                # Span is one. Use original target.
                self.vertex_set[current_source]["successors"].append(w)

                new_edge = current_source + "_" + w
                new_edge_object = deepcopy(old_edge)
                new_edge_object['source'] = current_source
                new_edge_object['target'] = w
                new_edge_object['class'] = 'q_edge'

                self.edge_set[new_edge] = new_edge_object
                self.vertex_set[w]['predecessors'].append(current_source)
                # self.layering[target_layer - 1].append(current_source)

    # Methods for tabulating edge crossings
    def __calculate_crossings__(self, action='return'):
        number_of_crossings = 0
        crossings = []
        for layer_index, layer in enumerate(self.layering):
            for index, v in enumerate(layer[:-1]):
                for u in layer[index + 1:]:
                    for y in self.vertex_set[v]['successors']:
                        for w in self.vertex_set[u]['successors']:
                            y_pos = self.vertex_set[y]['position_in_layer']
                            w_pos = self.vertex_set[w]['position_in_layer']

                            if y_pos > w_pos:
                                number_of_crossings += 1
                                crossings += [v + '_' + y + '---' + u + '_' + w]

        if action == 'return':
            return number_of_crossings
        elif action == 'set':
            self.number_of_crossings = number_of_crossings
        elif action == 'show_crossings':
            for i, crossing in enumerate(crossings):
                print(i + 1, '\t|\t', crossing)

    def __calculate_crossings_in_layer__(self, layer_index=0, action='return'):
        number_of_crossings = 0
        crossings = []
        layer = self.layering[layer_index]
        for index, v in enumerate(layer[:-1]):
            for u in layer[index + 1:]:
                for y in self.vertex_set[v]['successors']:
                    for w in self.vertex_set[u]['successors']:
                        y_pos = self.vertex_set[y]['position_in_layer']
                        w_pos = self.vertex_set[w]['position_in_layer']
                        if y_pos > w_pos:
                            number_of_crossings += 1
                            crossings += [v + '_' + y + '---' + u + '_' + w]

        if action == 'return':
            return number_of_crossings
        elif action == 'set':
            self.number_of_crossings = number_of_crossings
        elif action == 'show_crossings':
            for i, crossing in enumerate(crossings):
                print(i, '\t|\t', crossing)

    # Lets one look at crossings in either downward or upward direction
    def __crossings_between_layers__(self, top_layer, bottom_layer):
        # v --> y
        # u --> w
        pos = 'position_in_layer'
        crossings = 0
        for i, v in enumerate(top_layer):
            for u in top_layer[i:]:
                for y in [z for z in bottom_layer if z in self.vertex_set[v]['successors']]:
                    for w in [z for z in bottom_layer if z in self.vertex_set[u]['successors']]:
                        pos_v = top_layer.index(v)
                        pos_u = top_layer.index(u)
                        pos_y = bottom_layer.index(y)
                        pos_w = bottom_layer.index(w)

                        c_1 = pos_v < pos_u and pos_w < pos_y
                        c_2 = pos_v > pos_u and pos_w > pos_y

                        if c_1 or c_2:
                            crossings += 1
        return crossings

    # Median based crossing reduction heuristic
    def sugiyama_eades_wormald_modified_heuristic(self):
        counter = 1
        while counter:
            self.__sugiyama_eades_wormald_down__()
            self.__sugiyama_eades_wormald_up__()
            counter -= 1

    def __sugiyama_eades_wormald_down__(self):
        counter = 1
        while counter:
            for layer_index in range(len(self.layering) - 1):
                self.__sugiyama_eades_wormald_sort_of_next_layer__(layer_index)
            counter -= 1

    def __sugiyama_eades_wormald_up__(self):
        counter = 1
        while counter:
            for layer_index in range(len(self.layering) - 1, 0, -1):
                self.__sugiyama_eades_wormald_sort_of_previous_layer__(layer_index)
            counter -= 1

    def __sugiyama_eades_wormald_sort_of_next_layer__(self, layer_index):
        fixed_layer = self.layering[layer_index]
        free_layer = self.layering[layer_index + 1]

        current_crossings = self.__crossings_between_layers__(fixed_layer, free_layer)

        vertex_median_tuples = [self.__vertex_median__(vertex) for vertex in free_layer]
        vertex_median_tuples = sorted(vertex_median_tuples, key=lambda t: self.vertex_set[t[0]].in_degree())
        vertices_sorted_by_median_neighbor_position = sorted(vertex_median_tuples, key=lambda t: t[1])

        new_layer = [v for (v, m) in vertices_sorted_by_median_neighbor_position]

        new_crossings = self.__crossings_between_layers__(fixed_layer, new_layer)

        if new_crossings < current_crossings:
            self.__commit_new_layer__(layer_index + 1, new_layer)

    def __sugiyama_eades_wormald_sort_of_previous_layer__(self, layer_index):
        fixed_layer = self.layering[layer_index]
        free_layer = self.layering[layer_index - 1]

        current_crossings = self.__crossings_between_layers__(free_layer, fixed_layer)


        vertex_median_tuples = [self.__vertex_median__(vertex, direction='up') for vertex in free_layer]
        vertex_median_tuples = sorted(vertex_median_tuples, key=lambda t: self.vertex_set[t[0]].in_degree())
        vertices_sorted_by_median_neighbor_position = sorted(vertex_median_tuples, key=lambda t: t[1])

        new_layer = [v for (v, m) in vertices_sorted_by_median_neighbor_position]

        new_crossings = self.__crossings_between_layers__(new_layer, fixed_layer)

        if new_crossings < current_crossings:
            self.__commit_new_layer__(layer_index - 1, new_layer)

    # Update vertices in a layer
    def __commit_new_layer__(self, index, layer):
        self.layering[index] = layer
        for vertex in layer:
            self.vertex_set[vertex]['position_in_layer'] = layer.index(vertex)

    # Median position of vertex
    def __vertex_median__(self, vertex_key, direction='down', median_type='low', result='tuple'):
        median_pos = None
        if direction == 'down':
            median_pos = self.__vertex_median_down__(vertex_key, median_type)

        if direction == 'up':
            median_pos = self.__vertex_median_up__(vertex_key, median_type)

        if result == 'tuple':
            return (vertex_key, median_pos)
        else:
            return median_pos

    def __vertex_median_down__(self, vertex_key, median_type='low'):
        pos = 'position_in_layer'
        vertex_layer_index = self.vertex_set[vertex_key]['layer']
        median_pos = None

        fixed_layer = self.layering[vertex_layer_index - 1]
        free_layer = self.layering[vertex_layer_index]

        vertex_edges_to_fixed_layer = self.edge_set.select(**{'source': lambda s: s in fixed_layer,
                                                              'target': lambda t: t == vertex_key})
        neighbors_in_fixed_layer = [self.edge_set[edge]['source'] for edge in vertex_edges_to_fixed_layer]
        neighbors_positions = [self.vertex_set[neighbor][pos] for neighbor in neighbors_in_fixed_layer]

        if neighbors_positions:
            if median_type == 'low':
                median_pos = median_low(neighbors_positions)
            if median_type == 'high':
                median_pos = median_high(neighbors_positions)
        else:
            vertex_successors = self.vertex_set[vertex_key]['successors']
            if vertex_successors:
                if median_type == 'low':
                    median_pos = self.vertex_set.gather(vertex_successors).median_low_of(pos, 'subvalue')
                if median_type == 'high':
                    median_pos = self.vertex_set.gather(vertex_successors).median_high_of(pos, 'subvalue')
            else:
                median_pos = self.vertex_set[vertex_key][pos]
        return median_pos

    def __vertex_median_up__(self, vertex_key, median_type='low'):
        pos = 'position_in_layer'
        vertex_layer_index = self.vertex_set[vertex_key]['layer']
        median_pos = None

        free_layer = self.layering[vertex_layer_index]
        fixed_layer = self.layering[vertex_layer_index + 1]

        vertex_edges_to_fixed_layer = self.edge_set.select(**{'source': lambda s: s == vertex_key,
                                                              'target': lambda t: t in fixed_layer})
        neighbors_in_fixed_layer = [self.edge_set[edge]['target'] for edge in vertex_edges_to_fixed_layer]
        neighbors_positions = [self.vertex_set[neighbor][pos] for neighbor in neighbors_in_fixed_layer]

        if neighbors_positions:
            if median_type == 'low':
                median_pos = median_low(neighbors_positions)
            if median_type == 'high':
                median_pos = median_high(neighbors_positions)
        else:
            vertex_predecessors = self.vertex_set[vertex_key]['predecessors']
            if vertex_predecessors:
                if median_type == 'low':
                    median_pos = self.vertex_set.gather(vertex_predecessors).median_low_of(pos, 'subvalue')
                if median_type == 'high':
                    median_pos = self.vertex_set.gather(vertex_predecessors).median_high_of(pos, 'subvalue')
            else:
                median_pos = self.vertex_set[vertex_key][pos]

        return median_pos


    # Snake Method
    def __snake_crossings__(self, l_1=None, l_2=None, l_3=None, direction='down'):
        crossings = 0
        if direction == 'down':
            if l_3 == len(self.layering):
                crossings = self.__crossings_between_layers__(l_1, l_2)
            else:
                l_3 = self.layering[l_3]
                crossings = self.__crossings_between_layers__(l_1, l_2) + self.__crossings_between_layers__(l_2, l_3)

        else:
            if l_1 < 0:
                crossings = self.__crossings_between_layers__(l_2, l_3)
            else:
                l_1 = self.layering[l_1]
                crossings = self.__crossings_between_layers__(l_1, l_2) + self.__crossings_between_layers__(l_2, l_3)

        return crossings

    def __snake_up__(self, calling_vertex_key, predecessor_vertex_key, seen):
        calling_layer_index = self.vertex_set[calling_vertex_key]['layer']
        predecessor_layer_index = self.vertex_set[predecessor_vertex_key]['layer']


        calling_layer = self.layering[self.vertex_set[calling_vertex_key]['layer']]
        predecessor_layer = self.layering[self.vertex_set[predecessor_vertex_key]['layer']]

        calling_vertex_position = self.vertex_set[calling_vertex_key]['position_in_layer']
        predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']

        moves_made = []
        counter = 1
        while counter:
            for vertex in predecessor_layer:
                if vertex == predecessor_vertex_key:
                    continue


                for successor in self.vertex_set[vertex]['successors']:
                    vertex_position = self.vertex_set[vertex]['position_in_layer']
                    successor_position = self.vertex_set[successor]['position_in_layer']

                    condition_1 = successor_position > calling_vertex_position and vertex_position < predecessor_vertex_position
                    condition_2 = successor_position < calling_vertex_position and vertex_position > predecessor_vertex_position
                    condition_3 = (predecessor_vertex_key, vertex, predecessor_vertex_position,
                                   vertex_position) not in moves_made

                    if (condition_1 or condition_2) and condition_3:
                        current_crossings = self.__snake_crossings__(predecessor_layer_index - 1,
                                                                     predecessor_layer,
                                                                     calling_layer, 'up')


                        self.__swap__(predecessor_vertex_key, vertex)
                        predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']

                        new_crossings = self.__snake_crossings__(predecessor_layer_index - 1,
                                                                 predecessor_layer,
                                                                 calling_layer, 'up')

                        if new_crossings > current_crossings:
                            self.__swap__(predecessor_vertex_key, vertex)
                            predecessor_vertex_position = self.vertex_set[predecessor_vertex_key]['position_in_layer']


                        moves_made.append((predecessor_vertex_key, vertex, predecessor_vertex_position, vertex_position))
            counter -= 1

        for predecessor in self.vertex_set[predecessor_vertex_key]['predecessors']:
            if predecessor not in seen:
                seen.append(predecessor)
                self.__snake_up__(predecessor_vertex_key, predecessor, seen)

    def __snake_down__(self, calling_vertex_key, successor_vertex_key, seen):
        calling_layer_index = self.vertex_set[calling_vertex_key]['layer']
        successor_layer_index = self.vertex_set[successor_vertex_key]['layer']

        calling_layer = self.layering[self.vertex_set[calling_vertex_key]['layer']]
        successors_layer = self.layering[self.vertex_set[successor_vertex_key]['layer']]

        calling_vertex_position = self.vertex_set[calling_vertex_key]['position_in_layer']
        successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']

        moves_made = []
        counter = 1

        while counter:
            for other_vertex in successors_layer:
                if other_vertex == successor_vertex_key:
                    continue

                for predecessor in self.vertex_set[other_vertex]['predecessors']:
                    vertex_position = self.vertex_set[other_vertex]['position_in_layer']
                    predecessor_position = self.vertex_set[predecessor]['position_in_layer']

                    condition_1 = predecessor_position > calling_vertex_position and vertex_position < successor_vertex_position
                    condition_2 = predecessor_position < calling_vertex_position and vertex_position > successor_vertex_position
                    condition_3 = (successor_vertex_key, other_vertex, successor_vertex_position, vertex_position) not in moves_made

                    if (condition_1 or condition_2) and condition_3:
                        current_crossings = self.__snake_crossings__(calling_layer,
                                                                     successors_layer,
                                                                     successor_layer_index + 1)

                        self.__swap__(successor_vertex_key, other_vertex)

                        successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']

                        new_crossings = self.__snake_crossings__(calling_layer,
                                                                 successors_layer,
                                                                 successor_layer_index + 1)

                        if new_crossings > current_crossings:
                            self.__swap__(successor_vertex_key, other_vertex)
                            successor_vertex_position = self.vertex_set[successor_vertex_key]['position_in_layer']





                        moves_made.append((successor_vertex_key, other_vertex, successor_vertex_position, vertex_position))

            counter -= 1

        for successor in self.vertex_set[successor_vertex_key]['successors']:
            if successor not in seen:
                seen.append(successor)
                self.__snake_down__(successor_vertex_key, successor, seen)

    def snake_method(self):
        permutation_of_vertex_set = list(self.vertex_set.items())

        permutation_of_vertex_set = self.vertex_set.select(**{'predecessors': lambda l: len(l) == 0}) + self.vertex_set.select(**{'successors': lambda l: len(l) == 0})
        permutation_of_vertex_set = [(v, self.vertex_set[v]) for v in permutation_of_vertex_set]

        shuffle(permutation_of_vertex_set)

        down_seen = []
        up_seen = []

        counter = 1
        while counter:
            for vertex_key, vertex in permutation_of_vertex_set:
                for successor in vertex['successors']:
                    if successor not in down_seen:
                        down_seen.append(successor)
                        self.__snake_down__(vertex_key, successor, down_seen)
                for predecessor in vertex['predecessors']:
                    if predecessor not in up_seen:
                        up_seen.append(predecessor)
                        self.__snake_up__(vertex_key, predecessor, up_seen)

            counter -= 1

    def __swap__(self, vertex_1, vertex_2):
        position_1 = self.vertex_set[vertex_1]['position_in_layer']
        position_2 = self.vertex_set[vertex_2]['position_in_layer']

        self.vertex_set[vertex_1]['position_in_layer'] = position_2
        self.vertex_set[vertex_2]['position_in_layer'] = position_1

        layer_index = self.vertex_set[vertex_1]['layer']
        layer = self.layering[layer_index]

        layer[position_1] = vertex_2
        layer[position_2] = vertex_1

        self.layering[layer_index] = layer



    # Method for horizontal coordinates assignment (Brandes Koepf)
    def horizontal_coordinate_assignment(self, minimum_vertex_spacing):
        delta = minimum_vertex_spacing
        self.vertex_set.__make_brandes_kopf_keys__()
        self.edge_set.__make_crossing_type_keys__()

        directions = ["upper_left", "upper_right", "lower_left", "lower_right"]
        # directions = ['lower_right']

        self.__mark_type_1_conflicts__()

        for direction in directions:
            self.__vertical_alignment__(direction=direction)
            self.__horizontal_compaction__(delta, direction=direction)

        self.__balance_coordinate_assignments__(minimum_vertex_spacing, directions=directions)

        self.vertex_set.__remove_brandes_kopf_keys__()
        self.edge_set.__remove_crossing_types_keys__()

    def __eliminate_type_2_conflicts__(self):
        type_2_edges = self.edge_set.select(**{'source': lambda s: 'dummy' in self.vertex_set[s]['class'],
                                               'target': lambda t: 'dummy' in self.vertex_set[t]['class']})

        for i, e1 in enumerate(type_2_edges):
            for e2 in type_2_edges[i:]:
                if self.vertex_set[self.edge_set[e1]['source']]['layer'] != \
                        self.vertex_set[self.edge_set[e2]['source']]['layer']:
                    continue

                if self.vertex_set[self.edge_set[e1]['target']]['layer'] != \
                        self.vertex_set[self.edge_set[e2]['target']]['layer']:
                    continue

                p11 = self.vertex_set[self.edge_set[e1]['source']]['position_in_layer']
                p12 = self.vertex_set[self.edge_set[e1]['target']]['position_in_layer']
                p21 = self.vertex_set[self.edge_set[e2]['source']]['position_in_layer']
                p22 = self.vertex_set[self.edge_set[e2]['target']]['position_in_layer']

                if (p11 < p21 and p22 < p12) or (p11 > p21 and p22 > p12):
                    print('Type 2 crossing found between', e1, e2)
                    self.vertex_set[self.edge_set[e1]['target']]['position_in_layer'] = p22
                    self.vertex_set[self.edge_set[e2]['target']]['position_in_layer'] = p12
                    self.__eliminate_type_2_conflicts__()

    def __mark_type_1_conflicts__(self):
        # Type 0: non-inner segments cross. Resolved in greedly left fashion
        # Type 1: non-inner segment crosses an inner segment, resolve in favor of inner segment, marked by algorithm 1
        # Type 2: inner segments cross, mark as non-vertical and ignored for alignment

        # inner-segments will not appear on the first (0) or last layer (h); further this algorithm looks at layer i and
        #  i + 1. Thus no need to look at layer h-1, as it is resolved when calling h-2, where h is the number of layers

        # i = layer_index
        # h = number_of_layers
        # v_l_1_ip1 = v^(i+1)_(l_1)


        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        ct = crossing_types
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        number_of_layers = len(self.layering)
        for i in range(1, number_of_layers - 1):
            k_0 = -1
            l = 0  # index
            # maintains the upper neighbors v_k0_i and v_k1_i of the two closest inner segments
            for l_1 in range(len(self.layering[i+1])):

                v_l1_ip1 = self.layering[i + 1][l_1]
                condition_1 = l_1 == len(self.layering[i + 1]) - 1
                incident_edges = self.vertex_set.__inner_segments_of__(vertex_key=v_l1_ip1, which='predecessors')

                incident_edges = self.edge_set.select(**{'target': lambda t: t == v_l1_ip1 and self.vertex_set[t].dummy_q(),
                                                         'source': lambda s: self.vertex_set[s][l] == self.vertex_set[v_l1_ip1][l] - 1 and not self.vertex_set[s].dummy_q()
                                                         })

                if condition_1 or incident_edges:
                    k_1 = len(self.layering[i]) - 1

                    if incident_edges:
                        predecessors = self.vertex_set[v_l1_ip1]['predecessors'] # upper neighbors
                        k_1 = self.vertex_set[predecessors[0]][pos]

                    while l <= l_1:
                        # v_l_ip1 = self.layering[i + 1][l]
                        # other_predecessors = self.vertex_set.select(**{'successors': lambda s: v_l_ip1 in s,
                        #                                                'layer': lambda lay: lay == i})
                        # for predecessor in other_predecessors:
                        for predecessor in self.vertex_set[self.layering[i + 1][l]]['predecessors']:
                            k = self.layering[i].index(predecessor)

                            if predecessor not in self.layering[i]:
                                continue
                            if k < k_0 or k > k_1:
                                self.edge_set[predecessor + "_" + self.layering[i + 1][l]][ct]['type_1'] = True

                        l += 1
                    k_0 = k_1

    def __vertical_alignment__(self, direction="upper_left"):
        # Algorithm 2 from Brandes and Koepf
        # root and align (here called lower-/upper-neighbor) are initalized in vertex_set.__make_alignment_keys__()

        # i = layer_index
        # r = r
        # k = vertex_index
        align = 'align'
        bk = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        ct = crossing_types
        g = direction # g for greedy_direction
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        if direction == 'upper_left':
            for layer_index, layer in enumerate(self.layering):
                # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
                # indexed.
                r = -1

                for vertex_index, vertex in enumerate(layer):
                    v_k = vertex

                    # want median of ALL upper neighbors, not just those in the preceding layer
                    upper_neighbors = self.vertex_set[v_k]['predecessors']
                    # sort to ensure that they are in order as they appear in the layer
                    upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])

                    if upper_neighbors:
                        # d is number / index of upper neighbors, but our index is zero based.
                        d = len(upper_neighbors)
                        m_value = (d + 1) / 2

                        for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                            if self.vertex_set[v_k][bk][g][a] == v_k:
                                u_m = upper_neighbors[m]

                                u_m_v_k = u_m + '_' + v_k

                                condition_1 = self.edge_set[u_m_v_k][ct]['type_1']
                                condition_2 = r < self.vertex_set[u_m][pos]

                                if not condition_1 and condition_2:
                                    self.vertex_set[u_m][bk][g][a] = v_k
                                    self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                    self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                    r = self.vertex_set[u_m][pos]

        if direction == 'upper_right':
            for layer_index, layer in enumerate(self.layering):
                r = len(max(self.layering, key=lambda l: len(l)))

                for vertex in reversed(layer):
                    v_k = vertex

                    # want median of ALL upper neighbors, not just those in the preceding layer
                    upper_neighbors = self.vertex_set[v_k]['predecessors']
                    # sort to ensure that they are in order as they appear in the layer
                    upper_neighbors = sorted(upper_neighbors, key=lambda pred: self.vertex_set[pred][pos])



                    if upper_neighbors:
                        # d is number / index of upper neighbors, but our index is zero based.
                        d = len(upper_neighbors)
                        m_value = (d + 1) / 2

                        for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                            if self.vertex_set[v_k][bk][g][a] == v_k:
                                u_m = upper_neighbors[m]
                                u_m_v_k = u_m + '_' + v_k

                                condition_1 = self.edge_set[u_m_v_k][ct]['type_1']
                                condition_2 = r > self.vertex_set[u_m][pos]

                                if not condition_1 and condition_2:

                                    self.vertex_set[u_m][bk][g][a] = v_k
                                    self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                    self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                    r = self.vertex_set[u_m][pos]

        if direction == 'lower_left':
            for layer in reversed(self.layering):
                # r is 1 less than the starting index. In Brandes and Koepf they use 1 indexed arrays. Python is 0
                # indexed.
                r = -1

                for vertex_index, vertex in enumerate(layer):
                    v_k = vertex

                    lower_neighbors = self.vertex_set[v_k]['successors']
                    lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                    if lower_neighbors:
                        # d is number / index of upper neighbors, but our index is zero based.
                        d = len(lower_neighbors)
                        m_value = (d + 1) / 2

                        for m in [floor(m_value) - 1, ceil(m_value) - 1]:

                            if self.vertex_set[v_k][bk][g][a] == v_k:
                                u_m = lower_neighbors[m]
                                v_k_u_m = v_k + '_' + u_m

                                condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                                condition_2 = r < self.vertex_set[u_m][pos]

                                if not condition_1 and condition_2:
                                    self.vertex_set[u_m][bk][g][a] = v_k
                                    self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                    self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                    r = self.vertex_set[u_m][pos]

        if direction == 'lower_right':
            for layer in reversed(self.layering):
                r = len(max(self.layering, key=lambda l: len(l)))

                for vertex in reversed(layer):
                    v_k = vertex

                    lower_neighbors = self.vertex_set[v_k]['successors']
                    lower_neighbors = sorted(lower_neighbors, key=lambda succ: self.vertex_set[succ][pos])

                    if lower_neighbors:
                        # d is number / index of upper neighbors, but our index is zero based.
                        d = len(lower_neighbors)
                        m_value = (d + 1) / 2

                        for m in [ceil(m_value) - 1, floor(m_value) - 1]:
                            if self.vertex_set[v_k][bk][g][a] == v_k:
                                u_m = lower_neighbors[m]
                                v_k_u_m = v_k + '_' + u_m

                                condition_1 = self.edge_set[v_k_u_m][ct]['type_1']
                                condition_2 = r > self.vertex_set[u_m][pos]

                                if not condition_1 and condition_2:

                                    self.vertex_set[u_m][bk][g][a] = v_k
                                    self.vertex_set[v_k][bk][g][root] = self.vertex_set[u_m][bk][g][root]
                                    self.vertex_set[v_k][bk][g][a] = self.vertex_set[v_k][bk][g][root]
                                    r = self.vertex_set[u_m][pos]

    def __horizontal_compaction__(self, delta, direction='upper_left'):

        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        g = direction  # g for greedy_direction
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        if direction == 'upper_left':
            for vertex_key, vertex in self.vertex_set.items():
                if vertex[bk][g][r] == vertex_key:
                    self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

            for vertex_key, vertex in self.vertex_set.items():
                root_of_v = vertex[bk][g][r]

                vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                    vertex[bk][g][xy][x] += shift_sink_root_of_v

        if direction == 'upper_right':
            for vertex_key, vertex in self.vertex_set.items():
                if vertex[bk][g][r] == vertex_key:
                    self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

            for vertex_key, vertex in self.vertex_set.items():
                root_of_v = vertex[bk][g][r]

                vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                    vertex[bk][g][xy][x] += shift_sink_root_of_v


            min_x = min(self.vertex_set.values(), key=lambda v: v[bk][direction][xy][x])[bk][direction][xy][x]

            for vertex_key, vertex in self.vertex_set.items():
                vertex[bk][g][xy][x] += (min_x * -1)

        if direction == 'lower_left':
            for vertex_key, vertex in self.vertex_set.items():
                if vertex[bk][g][r] == vertex_key:
                    self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

            for vertex_key, vertex in self.vertex_set.items():
                root_of_v = vertex[bk][g][r]

                vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                    vertex[bk][g][xy][x] += shift_sink_root_of_v

        if direction == 'lower_right':
            for vertex_key, vertex in self.vertex_set.items():
                if vertex[bk][g][r] == vertex_key:
                    self.vertex_set.__place_block__(vertex_key, delta, direction, self.layering)

            for vertex_key, vertex in self.vertex_set.items():
                root_of_v = vertex[bk][g][r]

                vertex[bk][g][xy][x] = self.vertex_set[root_of_v][bk][g][xy][x]

                sink_root_of_v = self.vertex_set[root_of_v][bk][g][sk]
                shift_sink_root_of_v = self.vertex_set[sink_root_of_v][bk][g][sf]

                if vertex_key == root_of_v and not math.isinf(shift_sink_root_of_v):
                    vertex[bk][g][xy][x] += shift_sink_root_of_v


            min_x = min(self.vertex_set.values(), key=lambda v: v[bk][direction][xy][x])[bk][direction][xy][x]

            for vertex_key, vertex in self.vertex_set.items():
                vertex[bk][g][xy][x] += (min_x * -1)

    def __balance_coordinate_assignments__(self, delta=300, directions=None):
        self.minimum_vertex_separation = delta

        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        if directions is None:
            directions = ["upper_left", "upper_right", "lower_left", "lower_right"]
            directional_alignment_widths = {"upper_left": 0, "upper_right": 0, "lower_left": 0, "lower_right": 0}

        if directions is not None:
            directional_alignment_widths = {}
            for direction in directions:
                directional_alignment_widths[direction] = 0

        # Find smallest width
        for vertex_key, vertex_values in self.vertex_set.items():
            for direction in directions:
                if vertex_values[bk][direction][xy][x] > directional_alignment_widths[direction]:
                    directional_alignment_widths[direction] = vertex_values[bk][direction][xy][x]

        direction_with_smallest_width = min(directional_alignment_widths, key=directional_alignment_widths.get)
        smallest_direction_width = directional_alignment_widths[direction_with_smallest_width]

        directions_to_realign = [direction for direction in directions if direction != direction_with_smallest_width]

        # Align to smallest width
        for vertex_key, vertex_values in self.vertex_set.items():
            for direction in directions_to_realign:
                this_directions_width = directional_alignment_widths[direction_with_smallest_width]

                try:
                    scaling_factor = smallest_direction_width / this_directions_width
                except ZeroDivisionError:
                    scaling_factor = smallest_direction_width / (this_directions_width + 0.00000000001)
                vertex_values[bk][direction][xy][x] *= scaling_factor

        # Get average median
        average_median_coordinates = []

        for vertex_key, vertex_values in self.vertex_set.items():
            candidate_coordinates = []
            for direction in directions:
                candidate_coordinates.append(vertex_values[bk][direction][xy][x])
            candidate_coordinates.sort()

            k = len(directions)

            lower_median = floor((k + 1) / 2) - 1
            upper_median = ceil((k + 1) / 2) - 1

            average_median = (candidate_coordinates[lower_median] + candidate_coordinates[upper_median]) / 2
            average_median_coordinates.append((vertex_key, average_median))


        self.vertex_set.__make_coordinate_keys__()
        for vertex_key, x_coordinate in average_median_coordinates:
            self.vertex_set[vertex_key]["coordinates"]["x"] = x_coordinate
            self.vertex_set[vertex_key]["coordinates"]["y"] = self.vertex_set[vertex_key][layer] * delta


    # Reconstructing removed longed edges and getting coordinates of paths for smooth drawing
    def __get_p_and_q_edges_of_removed_edges__(self):
        p_edges = self.edge_set.select(**{'class': lambda c: c == 'p_edge'})
        q_edges = self.edge_set.select(**{'class': lambda c: c == 'q_edge'})

        matching_p_q_edges = []

        for p_edge in p_edges:
            p = int(p_edge.split('_')[-1])
            q_values = [int(q.split('_')[0]) for q in q_edges if int(q.split('_')[0]) >= p]

            differences = [q - p for q in q_values]
            q = q_values[q_values.index(min(differences) + p)]

            q_edge = [q_edge for q_edge in q_edges if int(q_edge.split('_')[0]) == q][0]

            matching_p_q_edges.append((p_edge, q_edge))

        return matching_p_q_edges

    def __get_all_edges_between_p_and_q_edges__(self, p_q_tuples):
        paths = []

        for (p, q) in p_q_tuples:

            if p.split('_')[-1] == q.split('_')[0]:
                paths.append([p, q])
                continue

            p_vertex = p.split('_')[-1]
            q_vertex = q.split('_')[0]

            next_edge = self.edge_set.select(**{'source': lambda s: s == p_vertex})[0]
            next_source, next_target = next_edge.split('_')[0], next_edge.split('_')[-1]

            path = [p]

            while next_target != q_vertex:
                path.append(next_edge)
                current_edge = next_edge
                current_source, current_target = next_source, next_target
                next_edge = self.edge_set.select(**{'source': lambda s: s == current_target})[0]
                next_source, next_target = next_edge.split('_')[0], next_edge.split('_')[-1]

            path.append(next_edge)
            path.append(q)

            paths.append(path)
        return paths

    def __get_coordinates_of_path__(self, path):
        coordinates = []
        for i, edge in enumerate(path):
            if i == 0:
                local_coordinates = self.vertex_set[edge.split('_')[0]]['coordinates']
                coordinates.append((local_coordinates['x'], local_coordinates['y']))
                local_coordinates = self.vertex_set[edge.split('_')[-1]]['coordinates']
                coordinates.append((local_coordinates['x'], local_coordinates['y']))
                continue

            local_coordinates = self.vertex_set[edge.split('_')[-1]]['coordinates']
            coordinates.append((local_coordinates['x'], local_coordinates['y']))
        return coordinates

    def __get_coordinates_of_paths__(self, paths):
        all_coordinates = []
        for path in paths:
            all_coordinates.append(self.__get_coordinates_of_path__(path))
        return all_coordinates

    def __long_paths__(self):
        p_q_edges = self.__get_p_and_q_edges_of_removed_edges__()
        p_q_edge_paths = self.__get_all_edges_between_p_and_q_edges__(p_q_edges)
        return p_q_edge_paths

    def __lists_of_edge_coordinates__(self):
        real_edges = self.edge_set.select(**{'class': lambda c: 'regular' in c})
        real_edges = [[edge] for edge in real_edges]
        long_edges = self.__long_paths__()
        return self.__get_coordinates_of_paths__(real_edges + long_edges)

    # Method for drawing the graph
    def __width_of_canvas__(self):
        max_x = max(self.vertex_set.values(), key=lambda v: v['coordinates']['x'])['coordinates']['x']
        x_digits = len(str(int(max_x)))
        x_size = max([round(max_x, -x_digits), int(str(int(str(max_x)[0]) + 1) + '0' * (x_digits - 1))])
        return x_size * 1.2

    def __vertex_radius_from_canvas_width__(self, width, spacing=4):
        return width / max([len(layer) for layer in self.layering]) / 2 / spacing

    def __reset_y_positions__(self):
        for vertex in self.vertex_set.values():
            vertex['coordinates']['y'] = vertex['layer'] * self.minimum_vertex_separation

    def __adjust_y_positions__(self, min_separation=2.5):
        width = self.__width_of_canvas__()
        node_radius = self.__vertex_radius_from_canvas_width__(width)

        self.__reset_y_positions__()
        for vertex in self.vertex_set.values():
            vertex['coordinates']['y'] = vertex['layer'] * (self.minimum_vertex_separation + node_radius * min_separation)

    def __height_of_canvas__(self):
        max_y = max(self.vertex_set.values(), key=lambda v: v['coordinates']['y'])['coordinates']['y']
        y_digits = len(str(int(max_y)))
        y_size = max([round(max_y, -y_digits), int(str(int(str(max_y)[0]) + 1) + '0' * (y_digits - 1))])
        return y_size * 1.2

    def __image_padding__(self, width, height, radius):
        max_x = max(self.vertex_set.values(), key=lambda v: v['coordinates']['x'])['coordinates']['x']
        max_y = max(self.vertex_set.values(), key=lambda v: v['coordinates']['y'])['coordinates']['y']

        x_padding = width - max_x
        y_padding = height - max_y

        # while max_y + y_padding / 2 + radius > height:
        #     height *= 1.01
        # y_padding = height - max_y

        return x_padding, y_padding

    def basic_workflow(self):
        self.remove_cycles()
        self.make_layering(implementation='demotion')
        self.generate_dummies(method='sugiyama')
        self.sugiyama_eades_wormald_modified_heuristic()
        self.snake_method()
        self.horizontal_coordinate_assignment(300)

        svg = self.make_svg()
        return svg




    def depth_first_search(self, start, stop, max_depth, number_of_paths_to_find):
        # self.vertex_set.add_subkey('depth_first_search_recursion', False)
        paths_found = []
        self.__depth_first_search_recursion__(start, stop, 1, max_depth, number_of_paths_to_find, [], paths_found)

        return paths_found

        # self.vertex_set.delete_subkey('depth_first_search_recursion')

    def __depth_first_search_recursion__(self, current, goal,
                                         current_depth, max_depth,
                                         number_of_paths_to_find, current_path, paths_found):
        # print(current_path, current, goal)
        current_path.append(current)
        # self.vertex_set[current]['depth_first_search_recursion'] = True
        if current_depth > max_depth:
            current_path.pop()
            return

        if current == goal:
            # print(current_path)
            if len(paths_found) < number_of_paths_to_find:
                paths_found.append(deepcopy(current_path))

            current_path.pop()
            return

        else:
            for successor in self.vertex_set[current]['successors']:
                # if not self.vertex_set[successor]['depth_first_search_recursion']:
                if self.vertex_set[successor].dummy_q():
                    self.__depth_first_search_recursion__(successor, goal, current_depth, max_depth, number_of_paths_to_find, current_path, paths_found)
                else:
                    self.__depth_first_search_recursion__(successor, goal, current_depth + 1, max_depth, number_of_paths_to_find, current_path, paths_found)


        current_path.pop()
        # self.vertex_set[current]['depth_first_search_recursion'] = False


    def __induce_layer__(self, layering):

        layering = [[str(v) for v in layer] for layer in layering]

        self.vertex_set.__update_subkeys_from_layering__(layering)
        self.layering = layering
        self.number_of_layers = len(layering)
        self.set_spans()


    def parse_paths(self, list_lists_of_paths):
        vertices = []
        edges = []
        for lists_of_paths in list_lists_of_paths:
            if lists_of_paths is []:
                continue
            for path in lists_of_paths:
                old_vertex = None
                for vertex in path:
                    if vertex not in vertices:
                        vertices.append(vertex)
                    if old_vertex is not None:
                        edges.append([old_vertex, vertex])
                    old_vertex = vertex
        return vertices, edges



    def extended_neighborhood(self, of, scope=1):
        neighborhood = [str(of)]
        used = []
        it = 0

        scopes = [[] for i in range(scope + 1)]
        scopes[0].append(str(of))
        for (i, neighborhood_scope) in enumerate(scopes):
            if i == len(scopes) - 1:
                break

            for neighbor in neighborhood_scope:
                successors = self.vertex_set[neighbor]['successors']
                predecessors = self.vertex_set[neighbor]['predecessors']
                new_neighbors = successors + predecessors
                for new_neighbor in new_neighbors:
                    if new_neighbor not in scopes[i+1]:
                        scopes[i + 1].append(new_neighbor)
        flattened_scopes = [item for local_scope in scopes for item in local_scope]
        return flattened_scopes













    def make_svg(self, filename='Magruder_et_al_2017_graph.svg', directory='Magruder_et_al_2017_graph_output'):
        # Calculate the width of the canvas
        width = self.__width_of_canvas__()
        # Determine the radius of the vertices from the canvas width
        radius = self.__vertex_radius_from_canvas_width__(width, 4)
        # Adjust y positions
        self.__adjust_y_positions__()
        # Calculate the height of the canvas
        height = self.__height_of_canvas__()
        # Determine padding for the canvas
        x_padding, y_padding = self.__image_padding__(width, height, radius)

        # Init an SVG object
        svg = SVG(radius, x_padding, y_padding)

        # Start the SVG file
        svg.start(height, width)

        # Define the arrow head and add it to the svg file
        arrowhead = svg.marker(refX=15, refY=5, orient='auto-start-reverse')
        svg.add(arrowhead)

        # Start a group element for the graph aspects
        svg.add(svg.start_group('graph', g_class='graph_container'))

        # Add vertices
        for vertex_key, vertex in self.vertex_set.items():

            # extract x and y coordinates
            x, y = vertex['coordinates']['x'], vertex['coordinates']['y']
            if vertex.dummy_q():
                # to display / style dummy vertices differently use this section
                continue

            # Start a group for the vertex
            group = svg.start_group(vertex_key, g_class='vertex')
            svg.add(group)

            # add circle for the vertex
            circle = svg.circle(x, y, svg.radius, stroke='black', stroke_width=3, fill='cyan', opacity=0.7)
            svg.add(circle)

            # text can also be added
            # text = svg.number(x, y, radius, vertex_key)
            # svg.add(text)

            # end the group
            svg.add(svg.end_group())

        # get coordinates of edges e.g. [ [(x1, y1), (x2, y2), (x3, y3)], [(x1, y1), (x2, y2)] ]
        coordinates_of_edges = self.__lists_of_edge_coordinates__()

        # extract "real" edge (edges that have no dummy vertices)
        real_edges = self.edge_set.select(**{'class': lambda c: 'regular' in c})
        real_edges = [[edge] for edge in real_edges]

        # seperate only the long edges
        long_edges = self.__long_paths__()
        all_edges = real_edges + long_edges


        # Add edges
        for ind, edge_coordinates in enumerate(coordinates_of_edges):

            # For showing direction of the marker
            marker_pos = 'end'
            orient = None

            # For labeling the edge
            edge_label = ''
            edge_key = ''

            # If the edge is a "real" edge
            if len(all_edges[ind]) == 1:
                # only one edge in the list, and it is an edge key
                edge_key = all_edges[ind][0]
                edge_label = self.edge_set[edge_key].connection_type()
                if edge_label is None:
                    edge_label = edge_key

            # If edge is a long edge
            else:
                edge_key = all_edges[ind][0]
                edge_label = self.edge_set[edge_key].connection_type()
                if edge_label is None:
                    source = all_edges[ind][0].split('_')[0]
                    target = all_edges[ind][-1].split('_')[-1]
                    edge_label = source + "_" + target

            # If edge is reverse, change marker position
            if self.edge_set[edge_key].reversed():
                marker_pos = 'start'

            # generate the path
            path = svg.path(edge_coordinates, stroke='black', stroke_width=svg.radius / 10,
                            marker=True, marker_position=marker_pos, marker_id='arrowhead', curved=True)

            # start group for the edge
            group = svg.start_group(edge_label, g_class='edge')
            svg.add(group)
            svg.add(path)
            svg.add(svg.end_group())
            # end the group
        svg.add(svg.end_group())
        svg.end()


        svg.write_out(filename, directory, True)
        return svg.file
