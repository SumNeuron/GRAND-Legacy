# Import the Graph class
from Graph import Graph

V = list(range(1,13))
E = [[1, 5], [2, 4], [3, 4],
     [4, 7], [5, 7], [5, 8], [5, 9], [6, 8],
     [7, 12], [8, 10], [8, 11]]


# V = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# E = [[1,5], [1,6], [2,5], [2,9], [2,8], [3,6], [3,8], [3,9], [4,5], [4,7], [4,9]]

G = Graph(vertices=V, edges=E)

G.layering = \
     [
          ['1', '2', '3'],
          ['4', '5', '6'],
          ['7', '8', '9'],
          ['10', '11', '12']
     ]

# G.layering =\
#     [
#         ['1', '2', '3', '4'],
#         ['5', '6', '7', '8', '9']
#     ]

for layer in G.layering:
    G.__update_vertices_layering_information__(layer)

for l, layer in enumerate(G.layering):
    for j, v in enumerate(layer):
        G.vertex_set[v]['layer'] = l

for vertex in V:
    G.vertex_set[str(vertex)]['class'] = 'regular'

G.edge_set.__make_dummy_keys__()

G.edge_set.__make_layering_keys__()
G.set_spans()

G.vertex_set.name_values()

# G.barycenter_method()

# G.__median_attempt__()
G.sugiyama_eades_wormald_modified_heuristic()
#G.snake_method()

# for i in range(len(G.layering) -1):
#     m_i = G.__digraph_interconnection_matrix_realization__(i)
#
#     print('x\t', [(k+1) + 3*(i+1) for k in range(len(G.layering[i+1]))])
#     print(' \t--------------')
#     for r, row in enumerate(m_i):
#         print((r+1)+(3*i),'\t',row)
#     print('\n\n')

# for i in [0]:
#     m_i = G.__digraph_interconnection_matrix_realization__(i)
#
#     print('x\t', [int(k) for k in G.layering[i+1]])
#     print(' \t--------------')
#     for r, row in enumerate(m_i):
#         print([int(k) for k in G.layering[i]][r],'\t',row)
#     print('\n\n')
#
#     a, b, c = G.__barycenter_method_for_interconnection_matrix__(m_i, G.layering[i], G.layering[i+1])
#
#     print('x\t',[int(k) for k in c])
#     print(' \t--------------')
#     for r, row in enumerate(a):
#         print([int(k) for k in b][r], '\t', row)
#     print('\n\n')
#
#     print(G.__row_barycenters_of_interconnection_matrix__(m_i), G.__row_barycenters_of_interconnection_matrix__(a))
#     print(G.__column_barycenters_of_interconnection_matrix__(m_i), G.__column_barycenters_of_interconnection_matrix__(a))
#
#
#     G.layering[i] = b
#     G.layering[i+1] = c
#     G.__update_vertices_layering_information__(G.layering[i])
#     G.__update_vertices_layering_information__(G.layering[i+1])

    # print(G.__crossings_in_interconnection_matrix__(m_i), G.__crossings_in_interconnection_matrix__(a))

# for i in [0, 1, 2]:
#     m_i = G.__digraph_interconnection_matrix_realization__(i)
#
#     print('x\t', [int(k) for k in G.layering[i+1]])
#     print(' \t--------------')
#     for r, row in enumerate(m_i):
#         print([int(k) for k in G.layering[i]][r],'\t',row)
#     print('\n\n')


# G.barycenter_method()
# G.reduce_crossings_with_generalized_sweep()
# G.snake_method()
G.horizontal_coordinate_assignment(100)
G.draw(file='/home/sumner/Desktop/test.svg')
