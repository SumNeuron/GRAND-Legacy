# Import the Graph class
from Graph import Graph

# Load demo graphs
from Demonstration_Graphs import demonstration_graphs

# Load function for loading external datasets
from ExternalGraphDatasets import retrieve_external_dataset
# Load external datasets as follows
# att = retrieve_external_dataset("att")
# rome = retrieve_external_dataset("rome")

# Use a demostration graph
V, E = demonstration_graphs['0'] # Brandes and Kopf's graph

# Alternatively define one of your own using shorthand
# V = list(range(1, 3 + 1))
# E = [
#     [1, 2], [1, 3],
#     [2, 1], [2, 3],
#     [3, 2], [3, 1]
# ]

# Initalize graph
G = Graph(vertices=V, edges=E)

# Remove cycles using the Tarjan modified Berger Shor method
G.remove_cycles()

# Construct a layering on the graph using a variant of longest path algorithm given by Healy and Nikolo
G.make_layering(implementation='demotion')

# Add dummy vertices and edges according to Sugiyama's seminal work
G.generate_dummies(method='sugiyama')

# Test Crossing Reduction
G.sugiyama_eades_wormald_modified_heuristic()
# Use snake method to improve crossing reduction
G.snake_method()

# Assign horizontal coordinate positions
G.horizontal_coordinate_assignment(300)

# Make the SVG
G.make_svg(filename='Magruder_et_al_2017_graph.svg', directory='Magruder_et_al_2017_graph_output')


# Test depth first search
v = '1'
w = '15'
depth = 10
num_paths = 10
dfs_paths = G.depth_first_search(start=v, stop=w, max_depth=depth, number_of_paths_to_find=num_paths)

print("DFS from {0} to {1} with max depth {2}".format(v, w, depth))
# dummy vertices will show, but not be counted in the depth!
for i, path in enumerate(dfs_paths):
    print("path ("+str(i)+"):\t", path)
