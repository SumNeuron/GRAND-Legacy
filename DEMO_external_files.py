from ExternalGraphDatasets import parse_graph_file
V, E = parse_graph_file("graphML_demo.txt")

from Graph import Graph
G = Graph(vertices=V, edges=E)
G.basic_workflow()
