from OrderedNestedDictionary import OrderedNestedDictionary
from Vertex import Vertex
import copy
import math
import queue

class VertexSet(OrderedNestedDictionary):
    def __init__(self, *args, **kwargs):

        super(VertexSet, self).__init__(*args, **kwargs)

    # Retrieve subset of vertices
    def p_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'p_vertex'})

    def q_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'q_vertex'})

    def r_vertices(self):
        return self.select(result='key', selection='every', **{'class': lambda c: c == 'r_vertex'})

    def dummy_vertices(self):
        dummies = []
        for id, vertex in self.items():
            if vertex.dummy_q():
                dummies.append(id)
        return dummies

    # Make preset keys
    def __make_layering_keys__(self):
        layering_keys = ['layer', 'position_in_layer']
        for key in set(layering_keys) - {'class'}:
            self.add_subkey(key, default=None)

    def __make_dummy_keys__(self):
        self.add_subkey('class', default='regular')

    def __make_edge_degree_keys__(self):
        edge_keys = ['in_degree', 'out_degree']
        for key in edge_keys:
            self.add_subkey(key, default=0)

    def __make_tarjan_keys__(self):
        tarjan_keys = ['tarjan_index', 'on_tarjan_stack', 'tarjan_low_link']
        for key in tarjan_keys:
            self.add_subkey(key)
        self.name_values()

    def __make_brandes_kopf_keys__(self):
        for key, vertex in self.items():
            vertex['brandes_kopf'] = {'upper_left': {'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},
                                      'upper_right':{'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},

                                      'lower_left': {'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}},

                                      'lower_right':{'root': key,
                                                     'align': key,
                                                     'sink': key,
                                                     'shift': float('inf'),
                                                     'coordinates': {'x': None, 'y': None}}
                                      }

    def __make_coordinate_keys__(self):
        self.add_subkey("coordinates", {"x": None, "y": None})

    # Remove preset keys
    def __remove_layering_keys__(self):
        layering_keys = ['layer', 'position_in_layer', 'class']
        for key in layering_keys:
            self.delete_subkey(key)

    def __remove_dummy_keys__(self):
        self.delete_subkey('class')

    def __remove_edge_degree_keys__(self):
        edge_keys = ['in_degree', 'out_degree']
        for key in edge_keys:
            self.delete_subkey(key)

    def __remove_tarjan_keys__(self):
        tarjan_keys = ['tarjan_index', 'on_tarjan_stack', 'tarjan_low_link', 'name']
        for key in tarjan_keys:
            self.delete_subkey(key)

    def __remove_brandes_kopf_keys__(self):
        self.delete_subkey('brandes_kopf')

    def __remove_coordinate_keys__(self):
        self.delete_subkey("coordinates")

    # Retrieve relative information from
    def __inner_segments_of__(self, vertex_key, which="all"):
        # layer = above, below or any
        incident_edges = []
        if self[vertex_key]['class'] == 'regular':
            pass
        else:
            if which == 'all':
                predecessors = self[vertex_key]['predecessors']
                successors = self[vertex_key]['successors']
                for predecessor in predecessors:
                    if self[predecessor]['class'] == 'regular':
                        continue
                    incident_edges.append(predecessor + "_" + vertex_key)

                for successor in successors:
                    if self[successor]['class'] == 'regular':
                        continue
                    incident_edges.append(vertex_key + "_" + successor)

            elif which == "predecessors":
                predecessors = self[vertex_key]['predecessors']
                for predecessor in predecessors:
                    if self[predecessor]['class'] == 'regular':
                        continue
                    incident_edges.append(predecessor + "_" + vertex_key)

            elif which == "successors":
                successors = self[vertex_key]['successors']
                for successor in successors:
                    if self[successor]['class'] == 'regular':
                        continue
                    incident_edges.append(vertex_key + "_" + successor)

        return incident_edges

    # def __upper_neighbors_of__(self, vertex_key, vertex_layer, layering):
    #     predecessors = self[vertex_key]['predecessors']
    #     upper_neighbors = []
    #     for predecessor in predecessors:
    #         if predecessor in layering[vertex_layer - 1]:
    #             upper_neighbors.append(predecessor)
    #     return upper_neighbors

    # Tarjan Strongly Connected Components
    def __tarjan_strongly_connected_components__(self):
        """
        VertexSet.__tarjan_strongly_connected_components__

        This determines the strongly connected components (SCCs) following the efficient travesal strategy laid out by
        Tarjan in 1986.

        """
        self.__make_tarjan_keys__()

        tarjan_index = 0
        stack = []
        strongly_connected_components = []
        for vertex_name, vertex_values in self.items():
            if vertex_values['tarjan_index'] == None:
                vertex_values.__strong_connect__(self, tarjan_index, stack, strongly_connected_components)


        # extract vertex keys from SCCs
        strongly_connected_components_names = [[] for i in range(len(strongly_connected_components))]

        for scc, strongly_connected_component in enumerate(strongly_connected_components):
            for component in strongly_connected_component:
                strongly_connected_components_names[scc].append(component['name'])

        self.__remove_tarjan_keys__()
        return strongly_connected_components_names

    # Make a layering of the Vertex Set
    def __assemble_layering_from_subkeys__(self, number_of_layers=None):
        min_layer = self.min_of('layer', result='subvalue')

        if min_layer < 0:
            for vertex in self:
                self[vertex]['layer'] += abs(min_layer)

        if number_of_layers is None:
            number_of_layers = self.max_of('layer', result='subvalue') + 1

        layering = [[] for i in range(number_of_layers)]

        for id, vertex in self.items():
            layering[vertex['layer']].append(id)
            vertex['position_in_layer'] = layering[vertex['layer']].index(id)

        return layering

    def __update_subkeys_from_layering__(self, layering):
        for i, layer in enumerate(layering):
            for j, vertex in enumerate(layer):
                self[vertex]['layer'] = i
                self[vertex]['position_in_layer'] = j

    def __make_layering__(self, method='healy_nikolov', implementation=''):
        _methods = ['healy_nikolov']
        _implementations = ['promotion', 'demotion', 'promote_layering']
        self.__make_layering_keys__()

        if method == 'healy_nikolov':
            number_of_layers = self.__healy_nikolov__()

        if method == 'depth_first_search':
            number_of_layers = self.__depth_first_search_layering__()

        implementations = implementation.split(' ')
        for imp in implementations:
            if imp == 'promote_layering':
                self.__nikolov_tarassov__()
                number_of_layers = self.max_of('layer', result='subvalue') + 1

            if imp == 'demotion':
                self.__root_demotion__()

            if imp == 'promotion':
                self.__leaf_promotion__()

        layering = self.__assemble_layering_from_subkeys__()
        return layering

    # Modified implementation of Healy and Nikolov's longest path layering algorithm
    def __healy_nikolov__(self):
        """
        See page 420-1 of Chapter 13: Hierarchical Graph Drawing by Healy and Nikolov.
        Modified to switch layering (e.g. layer 0 is up top instead of at the bottom)
        U = vertices assigned to a layer
        Z = all vertices assigned to a layer above the current
        V = Vertex Set
        v is selected from v in V \ U
        """
        U = set()
        Z = set()
        V = set(self.keys())

        current_layer = 0
        while U != V:

            choices = set(filter(lambda v: set(self[v]['predecessors']).issubset(Z), V - U))

            if choices:
                selected = choices.pop()
                self[selected]['layer'] = current_layer
                U.add(selected)

            else:
                current_layer += 1
                Z = Z.union(U)


        return current_layer + 1

    def __nikolov_tarassov__(self):
        layering = self.__assemble_layering_from_subkeys__()
        layering_back_up = layering
        promotions = 1
        while promotions:
            promotions = 0
            for vertex in self:
                if self[vertex].in_degree():
                    dummy_diff = self.__nikolov_tarassov_promote_node__(vertex)
                    if dummy_diff < 0:
                        promotions += 1
                        layering_back_up = copy.deepcopy(self.__assemble_layering_from_subkeys__())

                    else:
                        layering = copy.deepcopy(layering_back_up)
                        self.__update_subkeys_from_layering__(layering)
        self.__update_subkeys_from_layering__(layering)


    def __nikolov_tarassov_promote_node__(self, vertex):
        dummy_difference = 0
        predecessors = self[vertex]['predecessors']
        for predecessor in predecessors:
            if self[predecessor]['layer'] == self[vertex]['layer'] - 1:
                dummy_difference = dummy_difference + self.__nikolov_tarassov_promote_node__(predecessor)

        self[vertex]['layer'] -= 1
        if self[vertex]['layer'] < 0:
            diff = abs(self[vertex]['layer'])  # + 1
            for v in self:
                self[v]['layer'] += diff

        dummy_difference = dummy_difference - self[vertex].in_degree() + self[vertex].out_degree()
        return dummy_difference

    # Modified DFS layering algorithm
    def __depth_first_search_layering__(self):  # my own algorithm :)
        roots = self.select(**{'predecessors': lambda x: x == []})
        # roots = self.select('predecessors', selector=lambda x: x == [])


        ## If only predecessor is self, then vertex  is also a root
        for vertex in set(self).difference(set(roots)):
            if [vertex] == self[vertex]['predecessors']:
                roots.append(vertex)

        roots = sorted(roots, key=lambda r: len(self[r]['successors']) == 0)  # empty roots at end

        for root in roots:
            self[root]['layer'] = 0
            self.__depth_first_search_layering_recursion__(root)

        for vertex in self:  ## stop pure cycles / catch untouched nodes, a < -- > b disconnected from rest of graph
            if self[vertex]['layer'] == None:
                self.__depth_first_search_layering_recursion__(vertex)

        number_of_layers = 0
        for vertex in self:
            if self[vertex]['layer'] > number_of_layers:
                number_of_layers = self[vertex]['layer']
        return number_of_layers + 1

    def __depth_first_search_layering_recursion__(self, vertex):

        if not self[vertex]['layer'] == 0:
            predecessors = set(self[vertex]['predecessors']).difference({vertex})
            try:
                self[vertex]['layer'] = max(self.gather(predecessors).select(result='value',
                                                                             **{'layer': lambda x: type(x) == int})) + 1
            except ValueError:
                self[vertex]['layer'] = 0

        targets = self[vertex]['successors']
        # print(targets)
        targets = sorted(targets, key=lambda t: len(self[t]['successors']))
        targets.reverse()
        for target in targets:
            preds = self[target]['predecessors']
            if self.gather(preds).select(result='value', **{'layer': lambda x: x == None}) != []:
                continue

            if self[target]['layer'] == None:
                self.__depth_first_search_layering_recursion__(target)

    def __root_demotion__(self):
        leaves = self.select(**{'successors': lambda x: x == []})
        seen = []
        for leaf in leaves:
            self.__root_demotion_recursion__(leaf, seen)
        # for vertex in self.keys():
        #     self.__root_demotion_recursion__(vertex)

    def __root_demotion_recursion__(self, vertex, seen):
        if vertex in seen:
            return
        successors = self[vertex]['successors']
        if successors:
            max_layer = min(self.gather(successors).select(result='value',
                                                           **{'layer': lambda x: x is not None}))
            if max_layer - self[vertex]['layer'] > 1:
                self[vertex]['layer'] = max_layer - 1

        seen.append(vertex)
        for source in self[vertex]['predecessors']:
            if self[vertex]['layer'] - self[source]['layer'] > 1:
                self.__root_demotion_recursion__(source, seen)


    # def __root_demotion_recursion__(self, vertex):
    #     successors = self[vertex]['successors']
    #     if successors:
    #         max_layer = min(self.gather(successors).select(result='value',
    #                                                        **{'layer': lambda x: x is not None}))
    #         if max_layer - self[vertex]['layer'] > 1:
    #             self[vertex]['layer'] = max_layer - 1
    #
    #     for source in self[vertex]['predecessors']:
    #         if self[vertex]['layer'] - self[source]['layer'] > 1:
    #             self.__root_demotion_recursion__(source)


    def __root_demotion__(self):
        lowest_layer = self.max_of('layer', result='subvalue')
        bottom_vertices = self.select(**{'layer': lambda l: l == lowest_layer})
        for v in bottom_vertices:
            self.__root_demotion_bfs__(v)

    def __root_demotion_bfs__(self, v):
        q = queue.Queue()
        q.put(v)
        seen = [v]

        while not q.empty():
            current = q.get()
            successors = self[current]['successors']
            if successors:
                max_layer = min(self.gather(successors).select(result='value', **{'layer': lambda x: x is not None}))

                print(current, self[current]['layer'], max_layer)

                if max_layer - self[current]['layer'] > 1:
                    self[current]['layer'] = max_layer - 1

            pred = self[current]['predecessors']
            pred = sorted(pred, key=lambda p: self[p]['layer'])
            # reversed(pred)

            [(q.put(p), seen.append(p)) for p in pred if p not in seen]



    def __root_demotion__(self):
        vertices = sorted(list(self.keys()), key=lambda v: self[v]['layer'])

        while vertices:
            vertex = vertices.pop()
            successors = self[vertex]['successors']
            if successors:
                max_layer = min(self.gather(successors).select(result='value',
                                                               **{'layer': lambda x: x is not None}))
                if max_layer - self[vertex]['layer'] > 1:
                    self[vertex]['layer'] = max_layer - 1


    def __leaf_promotion__(self):
        # leaves = self.select(**{'successors': lambda x: x == []})
        # for leaf in leaves:
        #     self.__depth_first_search_layering_demotion_recursion__(leaf)
        for vertex in self.keys():
            self.__leaf_promotion_recursion__(vertex)

    def __leaf_promotion_recursion__(self, vertex):
        predecessors = self[vertex]['predecessors']

        if predecessors:
            lowest_layer = max(self.gather(predecessors).select(result='value',
                                                           **{'layer': lambda x: x is not None}))

            if self[vertex]['layer'] - lowest_layer > 1:
                self[vertex]['layer'] = lowest_layer + 1

        for target in self[vertex]['successors']:
            # print(vertex, source)
            if self[target]['layer'] - self[vertex]['layer'] > 1:
                self.__leaf_promotion_recursion__(target)

    # Linear Ordering algorithms for cycle breaking
    def __eades_et_al__(self):
        ordering = []
        for vertex_name, vertex_values in self.items():
            ordering.append([vertex_name, vertex_values.out_degree()])
        ordering = sorted(ordering, key=lambda x: x[1])
        ordering = [x[0] for x in ordering]
        return ordering

    def __magruder_magnitute__(self):
        ordering = list(self.keys())
        # ordering = sorted(ordering, key=lambda x: x["in_degree"] + x["out_degree"])
        # ordering.reverse()
        move_made = True
        while move_made:
            move_made = False
            for i in range(len(ordering) - 1):
                if self[ordering[i]].in_degree() > self[ordering[i + 1]].in_degree() \
                        and self[ordering[i]].in_degree() + self[ordering[i]].out_degree() == \
                                        self[ordering[i + 1]].in_degree() + self[ordering[i + 1]].out_degree():
                    temp = ordering[i + 1]
                    ordering[i + 1] = ordering[i]
                    ordering[i] = temp
                    move_made = True
        return ordering

    # Begin implementation of one-side crossing by Nagamochi
    def __cross__(self, v, w, layer):
        if self[v]['position_in_layer'] < self[w]['position_in_layer']:
            pass # c_vw
        else:
            pass # cvu

    # Brandes Köpf
    def __place_block__(self, vertex_key, delta, direction='upper_left', layering=None):

        align = 'align'
        brandes_kopf = 'brandes_kopf'
        coordinates = 'coordinates'
        crossing_types = 'crossing_types'
        layer = 'layer'
        position_in_layer = 'position_in_layer'
        root = 'root'
        shift = 'shift'
        sink = 'sink'

        a = align
        bk = brandes_kopf
        ct = crossing_types
        g = direction  # g for greedy_direction
        l = layer
        pos = position_in_layer
        r = root
        sf = shift
        sk = sink
        x = 'x'
        xy = coordinates

        v = vertex_key

        if direction == 'upper_left':
            if self[v][bk][g][xy][x] is None:
                self[v][bk][g][xy][x] = 0
                w = v

                w_is_v = False
                while not w_is_v:

                    pos_w = self[w][pos]
                    if pos_w > 0:

                        predecessor_of_w = layering[self[w][l]][pos_w - 1]
                        u = self[predecessor_of_w][bk][g][r]

                        self.__place_block__(u, delta, direction, layering)

                        sink_of_v = self[v][bk][g][sk]
                        sink_of_u = self[u][bk][g][sk]

                        x_v = self[v][bk][g][xy][x]
                        x_u = self[u][bk][g][xy][x]

                        if sink_of_v == v:
                            self[v][bk][g][sk] = sink_of_u
                            sink_of_v = self[v][bk][g][sk]

                        if sink_of_v != sink_of_u:
                            shift_sink_u = self[sink_of_u][bk][g][sf]
                            self[sink_of_u][bk][g][sf] = min([shift_sink_u, x_v - x_u - delta])

                        else:
                            self[v][bk][g][xy][x] = max([x_v, x_u + delta])

                    w = self[w][bk][g][a]
                    w_is_v = w == v

        if direction == 'upper_right':
            if self[v][bk][g][xy][x] is None:
                self[v][bk][g][xy][x] = 0

                w = v
                w_is_v = False
                while not w_is_v:

                    pos_w = self[w][pos]
                    if pos_w < len(layering[self[w][l]]) - 1:

                        successor_of_w = layering[self[w][l]][pos_w + 1]
                        u = self[successor_of_w][bk][g][r]

                        self.__place_block__(u, delta, direction, layering)

                        sink_of_v = self[v][bk][g][sk]
                        sink_of_u = self[u][bk][g][sk]

                        x_v = self[v][bk][g][xy][x]
                        x_u = self[u][bk][g][xy][x]


                        if sink_of_v == v:
                            self[v][bk][g][sk] = sink_of_u
                            sink_of_v = self[v][bk][g][sk]

                        if sink_of_v != sink_of_u:
                            shift_sink_u = self[sink_of_u][bk][g][sf]
                            self[sink_of_u][bk][g][sf] = max([shift_sink_u, x_v - x_u - delta])

                        else:
                            self[v][bk][g][xy][x] = min([x_v, x_u - delta])


                    w = self[w][bk][g][a]
                    w_is_v = w == v


        if direction == 'lower_left':
            if self[v][bk][g][xy][x] is None:
                self[v][bk][g][xy][x] = 0
                w = v

                w_is_v = False
                while not w_is_v:

                    pos_w = self[w][pos]
                    if pos_w > 0:

                        predecessor_of_w = layering[self[w][l]][pos_w - 1]
                        u = self[predecessor_of_w][bk][g][r]

                        self.__place_block__(u, delta, direction, layering)

                        sink_of_v = self[v][bk][g][sk]
                        sink_of_u = self[u][bk][g][sk]

                        x_v = self[v][bk][g][xy][x]
                        x_u = self[u][bk][g][xy][x]

                        if sink_of_v == v:
                            self[v][bk][g][sk] = sink_of_u
                            sink_of_v = self[v][bk][g][sk]

                        if sink_of_v != sink_of_u:
                            shift_sink_u = self[sink_of_u][bk][g][sf]
                            self[sink_of_u][bk][g][sf] = min([shift_sink_u, x_v - x_u - delta])

                        else:
                            self[v][bk][g][xy][x] = max([x_v, x_u + delta])

                    w = self[w][bk][g][a]
                    w_is_v = w == v

        if direction == 'lower_right':
            if self[v][bk][g][xy][x] is None:
                self[v][bk][g][xy][x] = 0
                w = v
                w_is_v = False
                while not w_is_v:

                    pos_w = self[w][pos]
                    if pos_w < len(layering[self[w][l]]) - 1:

                        successor_of_w = layering[self[w][l]][pos_w + 1]
                        u = self[successor_of_w][bk][g][r]

                        self.__place_block__(u, delta, direction, layering)

                        sink_of_v = self[v][bk][g][sk]
                        sink_of_u = self[u][bk][g][sk]

                        x_v = self[v][bk][g][xy][x]
                        x_u = self[u][bk][g][xy][x]


                        if sink_of_v == v:
                            self[v][bk][g][sk] = sink_of_u
                            sink_of_v = self[v][bk][g][sk]

                        if sink_of_v != sink_of_u:
                            shift_sink_u = self[sink_of_u][bk][g][sf]
                            self[sink_of_u][bk][g][sf] = max([shift_sink_u, x_v - x_u - delta])

                        else:
                            self[v][bk][g][xy][x] = min([x_v, x_u - delta])


                    w = self[w][bk][g][a]
                    w_is_v = w == v

    def neighborhood(self, vertex, scope=0, which='all'):
         predecessors = self[vertex]['predecessors']
         successors = self[vertex]['successors']

         neighbors = predecessors + successors

         last_index = 0
         while scope:
             for current_vertex in neighbors[last_index:]:
                 current_predecessors = self[current_vertex]['predecessors']
                 current_successors = self[current_vertex]['successors']

                 predecessors += current_predecessors
                 successors += current_successors

                 neighbors += current_predecessors
                 neighbors += current_successors
                 last_index += 1

         neighborhood = list(set(neighbors))

         if which == 'predecessors':
             neighborhood = list(set(predecessors))
         elif which == 'successors':
             neighborhood = list(set(successors))


         return neighborhood
