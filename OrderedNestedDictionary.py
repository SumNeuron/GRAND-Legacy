from collections import OrderedDict
from operator import itemgetter, attrgetter
from statistics import mean, median, median_high, median_low
from copy import deepcopy

# OrderedNestedDictionary is a class meant for handling an ordered dictionary of dictionaries i.e. for each key-value
# pair in OrderedNestedDictionary, the value is a dictionary (or derived therefrom). Furthermore, this class is built
# with the presumption that every value of the contained key-value pairs shares a similar 'template' (mentioned below)
# In discourse of the OrderedNestedDictionary class one refers to the keys and values of the outer-most dictionary as
# such, for each nesting one prepends sub prior to the term key and value. e.g. an OrderedNestedDictionary class
# contains key-value pairs such that every value is itself a dictionary. Given the value (a dictionary) of a key-value
# pair belonging to the OrderedNestedDictionary class, that value consists of sub-key -- sub-value pairs. We say that
# the values of the OrderedNestedDictionary share a template if and only if, every value in the key-value pairs, share
# the same sub-keys.
#
# { 'key_1': # value_1 is a template-dictionary
#            {'sub_key_1': sub_value_1,
#             'sub_key_2': sub_value_2,
#             'sub_key_3': sub_value_3},
#   'key_2': # value_2
#            {'sub_key_1': sub_value_1,
#             'sub_key_2': sub_value_2,
#             'sub_key_3': sub_value_3}
#   'key_3': # value_3
#            {'sub_key_1': sub_value_1,
#             'sub_key_2': sub_value_2,
#             'sub_key_3': sub_value_3}
# } # end of OrderedNestedDictionary
# NOTE: although 'sub_key_1', 'sub_key_2', and 'sub_key_3' are equivalent for value_1, value_2, and value_3 (hence
# defining a template) the corresponding sub_values may change per instance
#

class OrderedNestedDictionary(OrderedDict):
    def __init__(self, *args, **kwds):
        super(OrderedNestedDictionary, self).__init__(*args, **kwds)


    def at(self, i):
        """
        at(i) retrieves the key of element in the ith position in the ordered dictionary.
        """
        return self.__getitem__(list(self.keys())[i])

    # first, last, most, and rest are core functions lifted from the Wolfram Language. They may prove useful for
    # some computation across dictionary elements since they are ordered.

    @property
    def last(self):
        """
        last() returns the last key in the ordered dictionary.
        """
        return self.at(-1)

    @property
    def first(self):
        """
        first() returns the first key in the ordered dictionary
        """
        return self.at(0)

    @property
    def most(self):
        """
        most() returns all but the last key in the ordered dictionary
        """
        most = [(key, value) for key, value in list(self.items())[0:-1]]
        return most

    @property
    def rest(self):
        """
        rest() returns all but the first key in the ordered dictionary
        """
        rest = [(key, value) for key, value in list(self.items())[1:]]
        return rest

    @property
    def member_q(self, key):
        """
        member_q(key) returns a boolean as to whether the passed key is found in the ordered dictionary
        """
        if key in list(self.keys()):
            return True
        return False

    # gather and select work similarly to reap and sow operations. select can be used to obtain the keys which have
    # values (which are dictionaries) that have various sub-key -- sub-value pairs matching specified criteria. gather
    # takes a list of such keys and makes a new instance of OrderedNestedDictionary containing just those passed keys.
    # Together they provide a convenient way to filter via encapsulation e.g.
    #
    #     OrderedNestedDictionary.gather(OrderedNestedDictionary.select(result='key',
    #                                                                   selection='every',
    #                                                                   **{'sub_key_1':lambda l: l == condition}
    #                                                                  ))
    def gather(self, keys):
        """
        gather(keys) takes an array of keys and creates a new instance of OrderedNestedDictionary consisting of only
        those keys found in the calling instance.
        """
        collected = OrderedNestedDictionary()
        for key in keys:
            try:
                collected[key] = self[key]
            except KeyError:
                None
        return collected

    def select(self, result='key', selection='every', **kwargs):
        """
        select(result:selection:**kwargs) filters the calling OrderedNestedDictionary according to which values match
        the conditions specified by the **kwargs.

        :param result: can be either 'key', 'value' or 'key_value'. result dictates whether to return the key, the
        value, or both of those elements which satisfied the conditions specified by **kwargs.

        :param selection: can be either 'every' or 'any'. Works as a local logical AND or logical OR. If more than one
        condition is given in **kwargs will return the element (as specified by result) if 'every' or 'any' of the
        passed conditions are meet.

        :param kwargs: an arbitrary number of selectors. The form should be of sub-key--selector where sub-key is part
        of the template (e.g. a sub-key found in every value of the calling OrderedNestedDictionary). The selector
        should be a lambda function.
        """

        # to handle multi-keys currently testing for if the selector is actually a list
        # future / better imlpementations may have a set of tuples, or two lists one for args and one for selecetors
        # or 'descriptor': ('key', selector)



        selected = []
        for key, value in self.items():
            keep = False
            for subkey, selector in kwargs.items():
                if type(selector) is list:
                    for subselector in selector:
                        try:
                            if not subselector(value[subkey]):
                                if selection == 'every':
                                    keep = False
                                    break
                                elif selection == 'any':
                                    pass
                            else:
                                keep = True
                        except KeyError:
                            pass
                else:
                    try:
                        if not selector(value[subkey]):
                            if selection == 'every':
                                keep = False
                                break
                            elif selection == 'any':
                                pass
                        else:
                            keep = True
                    except KeyError:
                        pass
            if keep:
                if result == 'key':
                    selected.append(key)
                elif result == 'value':
                    selected.append(value[subkey])
                elif result == 'key_value':
                    selected.append((key, value[subkey]))

        return selected

    def sort_by(self, subkey, reverse=False):
        # self = OrderedNestedDictionary(sorted(iter(self.items()), key=lambda item: item[1][subkey], reverse=reverse))
        # print(self)
        post_sort = sorted(iter(self.items()), key=lambda item: item[1][subkey], reverse=reverse)
        for key, value in post_sort:
            super(OrderedNestedDictionary, self).move_to_end(key)

        return None

    # the following x_of functions are not given descriptions as they as general description serves them all. Given a
    # sub-key in the template for all values of the OrderedNestedDictionary, x_of applies whatever function x is to the
    # sub-value of each value's sub-key.

    def min_of(self, subkey, result='key'):
        key, value = min(iter(self.items()), key=lambda item: item[1][subkey])
        subvalue = value[subkey]
        if result == 'key':
            return key
        elif result == 'value':
            return value
        elif result == 'subvalue':
            return subvalue
        elif result == 'key_value':
            return key, value
        elif result == 'key_subvalue':
            return key, subvalue
        return None


    def max_of(self, subkey, result='key'):
        key, value = max(iter(self.items()), key=lambda item: item[1][subkey])
        subvalue = value[subkey]
        if result == 'key':
            return key
        elif result == 'value':
            return value
        elif result == 'subvalue':
            return subvalue
        elif result == 'key_value':
            return key, value
        elif result == 'key_subvalue':
            return key, subvalue
        return None

    def mean_of(self, subkey):
        return mean([value[subkey] for value in self.values()])

    def median_of(self, subkey):
        return median([value[subkey] for value in self.values()])

    def median_low_of(self, subkey, result='key'):
        values = [value[subkey] for value in self.values()]
        subvalue = median_low(values)
        index = values.index(subvalue)
        key = list(self.keys())[index]
        value = self.at(index)
        if result == 'subvalue':
            return subvalue
        elif result == 'value':
            return value
        elif result == 'key':
            return key
        elif result == 'key_subvalue':
            return key, subvalue
        elif result == 'key_value':
            return key, value
        return None


    def median_high_of(self, subkey, result='key'):
        values = [value[subkey] for value in self.values()]
        subvalue = median_high(values)
        index = values.index(subvalue)
        key = list(self.keys())[index]
        value = self.at(index)
        if result == 'subvalue':
            return subvalue
        elif result == 'value':
            return value
        elif result == 'key':
            return key
        elif result == 'key_subvalue':
            return key, subvalue
        elif result == 'key_value':
            return key, value
        return None


    # add_/delete_subkey adds / deletes the subkey into every value in the OrderedNestedDictionary
    def add_subkey(self, subkey, default=None):
        for key, value in self.items():
            if subkey not in value.keys():
                value[subkey] = deepcopy(default)

    def delete_subkey(self, subkey):
        for key, value in self.items():
            try:
                del value[subkey]
            except KeyError:
                pass

    def name_values(self):
        """
        name_values generates a key, 'name' for each value, such that the corresponding value of the new key is the key
        of the value (thereby allowing one, given only the value, to know the associated key).
        """
        for key, value in self.items():
            value['name'] = key
